package sk.oliver89.basicexowrapper;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;
import sk.oliver89.basicexowrapper.media.BasicExoWrapper;
import sk.oliver89.basicexowrapper.model.StoriesRepository;
import sk.oliver89.basicexowrapper.model.StoryItem;
import sk.oliver89.basicexowrapper.timertask.ImageTimerTask;
import sk.oliver89.basicexowrapper.timertask.StoryTimerTask;
import sk.oliver89.basicexowrapper.timertask.StoryTimerTaskCallback;
import sk.oliver89.basicexowrapper.timertask.VideoTimerTask;

/**
 * Created by olive on 11/24/2017.
 */

public class StoryFragment extends Fragment implements BasicExoWrapper.Listener, View.OnTouchListener, StoryTimerTaskCallback {

    private static final String TAG = StoryFragment.class.getCanonicalName();
    private static final String EXTRA_STORY_ID = "EXTRA_STORY_ID";
    private static final String EXTRA_CURRENT_MEDIA_INDEX = "EXTRA_CURRENT_MEDIA_INDEX";
    private static final String EXTRA_CURRENT_MEDIA_SAVED_POSITION = "EXTRA_CURRENT_MEDIA_SAVED_POSITION";

    private static final int PAUSE_THRESHOLD = 200;
    private static final int EMPTY_PROGRESS = 0;
    private static final int FULL_PROGRESS = 100;
    private static final int TIMER_DELAY = 16;
    private static final int TIMER_PERIOD = 16;
    private static final int IMAGE_DISPLAY_DURATION = 4000;

    //region Members

    /**
     * Position of this story in the entire feed of stories
     */
    private int mStoryIndex;

    /**
     * Position of currently displayed video in this story
     */
    private int mCurrentMediaIndex;

    /**
     * Current item's playback position to resume playback after screen lock, home button,
     * or configuration change. The value is updated in onPause(), persisted in onSaveInstanceState()
     * and cleared when playback starts again.
     */
    private long mCurrentMediaSavedPosition;

    /**
     * A flag that gets set when the last media reaches its end
     * (either naturally or by user tapping); basically indicating
     * that this fragment is done playing
     */
    private boolean mStoryPlaybackEnded;

    /**
     * List of string URLs of the videos in this story
     */
    private List<StoryItem> mMedia;

    /**
     * URL of the currently loaded video
     */
    private String loadedMediaUrl;

    private boolean mPlaybackStartRequested;

    private boolean mMediaReady;

    // utility
    private Handler mHandlerMain;
    private long timePressedDown;
    private Timer playbackProgressTimer = new Timer();
    private StoryTimerTask playbackProgressTimerTask;

    private BasicExoWrapper exoPlayer;

    //region UI

    @BindView(R.id.img_story_picture)
    ImageView imgPicture;
    @BindView(R.id.surface_story_video)
    SurfaceView surfaceVideo;
    @BindView(R.id.txt_story_name)
    TextView txtStoryName;
    @BindView(R.id.group_story_bars)
    LinearLayout layoutBars;
    @BindView(R.id.progress_story_loading)
    ProgressBar progressLoading;
    @BindView(R.id.view_story_goto_next)
    View btnNextItem;
    @BindView(R.id.view_story_goto_previous)
    View btnPreviousItem;

    //endregion

    //endregion

    public static StoryFragment newInstance(int storyId) {
        StoryFragment fragment = new StoryFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_STORY_ID, storyId);
        // TODO REMOVE args.putInt(EXTRA_MEDIA_COUNT, mediaCount);
        fragment.setArguments(args);
        return fragment;
    }

    //region Lifecycle

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mStoryIndex = getArguments().getInt(EXTRA_STORY_ID);
        mHandlerMain = new Handler(Looper.getMainLooper());

        if (savedInstanceState != null) {
            mCurrentMediaIndex = savedInstanceState.getInt(EXTRA_CURRENT_MEDIA_INDEX);
            mCurrentMediaSavedPosition = savedInstanceState.getLong(EXTRA_CURRENT_MEDIA_SAVED_POSITION);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_story, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        progressLoading.setVisibility(View.GONE);
        // currently we're using the playback progress bars in indeterminate mode to show loading state

        exoPlayer = new BasicExoWrapper(getContext(), surfaceVideo);
        exoPlayer.setLogIdentifier(mStoryIndex);
        exoPlayer.setListener(this);
        btnNextItem.setOnTouchListener(this);
        btnPreviousItem.setOnTouchListener(this);
        // TODO check if listeners are removed

        displayStoryInfo();
        loadStoryMedia();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            requestStartPlayback();
        } else {
            stopPlayback();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getStreamActivity().getCurrentPage() == mStoryIndex) {
            Log.d(makeLogTag(), "window visible; resuming playback");
            // start playback after a short delay
            mHandlerMain.postDelayed(this::requestStartPlayback, 500);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getStreamActivity().getCurrentPage() == mStoryIndex) {
            Log.i(makeLogTag(), "window hidden; pausing playback");
            mCurrentMediaSavedPosition = getCurrentPlaybackPosition();
            pauseCurrentMedia();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EXTRA_CURRENT_MEDIA_INDEX, mCurrentMediaIndex);
        outState.putLong(EXTRA_CURRENT_MEDIA_SAVED_POSITION, mCurrentMediaSavedPosition);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        stopPlayback();
        exoPlayer.removeListener();
        exoPlayer.release();
    }

    //endregion

    //region Public methods

    /**
     * Request to start playback in this fragment right away.
     * If media not yet loaded, fragment remembers this request and starts playback when ready.
     */
    public void requestStartPlayback() {
        Log.i(TAG, "playback will start in timeline #" + mStoryIndex);
        if (mMediaReady) {
            Log.d(TAG, "playback start requested, media ready, starting");
            startPlayback();
        } else {
            Log.d(TAG, "playback start requested, media not ready, waiting...");
            mPlaybackStartRequested = true;
        }
    }

    /**
     * Stops playback, unloads media, stops all timers.
     * Should be called when this fragment instance is hidden
     * (swiped to different page, etc)
     */
    public void stopPlayback() {
        Log.i(makeLogTag(), "playback stopped in timeline #" + mStoryIndex);
        stopTimers();
        emptyAllBars();
        if (exoPlayer != null) {
            exoPlayer.pause();
        }
        loadedMediaUrl = null;
        mCurrentMediaIndex = 0;
    }

    public boolean isCurrentMediaVideo() {
        if (hasAnyMedia()) {
            return mMedia.get(mCurrentMediaIndex).isVideo();
        }
        return false;
    }

    //endregion

    //region Private methods

    private MainActivity getStreamActivity() {
        return (MainActivity) this.getActivity();
    }

    private boolean hasAnyMedia() {
        return mMedia != null && !mMedia.isEmpty();
    }

    private void loadStoryMedia() {
        // TODO use an AsyncTask with delay
        mMedia = StoriesRepository.getStory(mStoryIndex);
        initProgressBars();
        onMediaLoaded();
    }

    // TODO rename
    private void onMediaLoaded() {
        Log.d(TAG, "media loaded");
        mMediaReady = true;
        if (mPlaybackStartRequested) {
            startPlayback();
        }
    }

    //region Media loading and playback

    /**
     * Starts playback of this story
     */
    private void startPlayback() {
        if (hasAnyMedia()) {
            Log.d(makeLogTag(),
                    "starting auto playback from item #" + mCurrentMediaIndex + " (zero-based)");
            emptyProgressBar(mCurrentMediaIndex, false);
            for (int i = 0; i < mCurrentMediaIndex; i++) {
                fillProgressBar(i, false);
            }
            mStoryPlaybackEnded = false;
            displayCurrentMediaInfo();
            loadCurrentMedia();
        }
    }

    /**
     * Loads the current media if not already loaded.
     * When ready, playback starts automatically (see onPrepared)
     */
    private void loadCurrentMedia() {

        try {
            if (hasAnyMedia() && getContext() != null) {
                Log.d(makeLogTag(), makeLogMessage("loading; type: " + (isCurrentMediaVideo()
                        ? "VIDEO" : "IMAGE" + ", url: " + mMedia.get(mCurrentMediaIndex).getUrl())));
                Uri mediaUri = Uri.parse(mMedia.get(mCurrentMediaIndex).getUrl());
                toggleLoadingProgress(true);
                toggleSurface(isCurrentMediaVideo());
                if (isCurrentMediaVideo()) {
                    // TODO display video thumbnail while it's loading
                    exoPlayer.load(mediaUri, false);
                } else {
                    toggleSurface(false);
                    Glide.with(getContext())
                            .load(mediaUri)
                            .listener(listenerGlide)
                            .into(imgPicture);
                }
                // the playback will play automatically in onPrepared
            } else {
                Log.w(makeLogTag(), "there is no media to play!");
            }
        } catch (Exception e) {
            Log.e(makeLogTag(), makeLogMessage("error loading current media: " + e.toString()));
            skipCurrentItemOnError();
        }
    }

    private void playCurrentMedia() {

        if (!hasAnyMedia()) {
            return;
        }

        Log.d(makeLogTag(), makeLogMessage("PLAY current media"));

        if (playbackProgressTimerTask == null) {
            initTimerForCurrentProgress();
            Log.w(makeLogTag(), makeLogMessage("playCurrentMedia() had to init TimerTask even though it should be ready"));
            // NOTE: call to initTimerForCurrentProgress is meant to happen outside this method,
            // explicitly, when loading NEW media (next/prev item), but not when RESUMING playback
        }

        // initTimerForCurrentProgress();
        // TODO when restoring image playback after app in background, resume progress, don't start over
        if (isCurrentMediaVideo()) {
            surfaceVideo.setVisibility(View.VISIBLE);
            if (mCurrentMediaSavedPosition > 0) {
                exoPlayer.seekTo(mCurrentMediaSavedPosition);
            }
            exoPlayer.play();
        } else {
            toggleImagePaused(false);
        }

        mCurrentMediaSavedPosition = 0;

    }

    /**
     * Rewinds current item to the beginning in case
     * current playback position is greater than 1000ms.
     * @return Whether item was rewinded.
     */
    private boolean rewindCurrentMedia() {
        if (isCurrentMediaVideo()) {
            if (exoPlayer.getCurrentPosition() > 1000) {
                emptyProgressBar(mCurrentMediaIndex, true);
                exoPlayer.rewind(true);
                return true;
            }
            return false;
        }
        else {
            ImageTimerTask imageTimerTask = (ImageTimerTask) playbackProgressTimerTask;
            if (imageTimerTask != null && imageTimerTask.getCurrentPosition() > 1000) {
                imageTimerTask.rewind();
                return true;
            }
            return false;
        }
    }

    private void pauseCurrentMedia() {
        Log.d(makeLogTag(), makeLogMessage("PAUSE current media"));
        if (hasAnyMedia()) {
            if (isCurrentMediaVideo()) {
                exoPlayer.pause();
            } else {
                toggleImagePaused(true);
            }
        }
    }

    /**
     * "Pauses" or "Plays" the image file by controlling
     * the associated ImageTimerTask instance
     * @param isPaused
     */
    private void toggleImagePaused(boolean isPaused) {
        if (playbackProgressTimerTask != null && playbackProgressTimerTask instanceof ImageTimerTask) {
            ((ImageTimerTask) playbackProgressTimerTask).togglePaused(isPaused);
        } else {
            Log.w(makeLogTag(), makeLogMessage("tried to pause/play an image when a video seems to be loaded"));
        }
    }

    /**
     * Tries navigating to next media in stream.
     * If no next media,
     *
     * @return
     */
    private boolean tryGoToNextMedia() {

        Log.v(makeLogTag(), makeLogMessage("tryGoToNextMedia"));

        if (mMedia == null) {
            return false;
        }

        if (mCurrentMediaIndex < mMedia.size() - 1) {
            // if next media available, load & play it
            moveToNextMedia();
            return true;
        } else {
            // inform activity that stream has ended
            Log.d(makeLogTag(), makeLogMessage("no more media in stream"));
            mStoryPlaybackEnded = true;
            if (getStreamActivity() != null) {
                getStreamActivity().onStoryEnded(mStoryIndex);
            }
            return false;
        }

    }

    private boolean tryGoToPreviousMedia() {

        Log.v(makeLogTag(), makeLogMessage("tryGoToPreviousMedia"));
        if (mMedia == null) {
            return false;
        }
        if (mCurrentMediaIndex - 1 >= 0) {
            moveToPreviousMedia();
            return true;
        }
        Log.d(makeLogTag(), makeLogMessage("this is the first item"));
        return false;

    }

    private void moveToNextMedia() {
        Log.v(makeLogTag(), makeLogMessage("moveToNextMedia"));
        stopTimers();
        fillProgressBar(mCurrentMediaIndex, true);
        mCurrentMediaIndex++;
        mCurrentMediaSavedPosition = 0;
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {
                displayCurrentMediaInfo();
                loadCurrentMedia();
            });
        }
    }

    private void moveToPreviousMedia() {
        Log.v(makeLogTag(), makeLogMessage("moveToPreviousMedia"));
        stopTimers();
        emptyProgressBar(mCurrentMediaIndex, true);
        mCurrentMediaIndex--;
        mCurrentMediaSavedPosition = 0;
        emptyProgressBar(mCurrentMediaIndex, true);
        displayCurrentMediaInfo();
        loadCurrentMedia();
    }

    private void skipCurrentItemOnError() {
        toggleLoadingProgress(false);
        Toast.makeText(getContext(), "Playback error; skipping", Toast.LENGTH_LONG).show();
        mHandlerMain.postDelayed(this::tryGoToNextMedia, 1000);
    }

    /**
     * Return the current item's playback position,
     * whether it's a video or an image
     */
    private long getCurrentPlaybackPosition() {
        long position = 0;
        if (!hasAnyMedia())
            return position;
        if (isCurrentMediaVideo()) {
            position = exoPlayer.getCurrentPosition();
        } else {
            if (playbackProgressTimerTask instanceof ImageTimerTask) {
                position = ((ImageTimerTask) playbackProgressTimerTask).getCurrentPosition();
            }
        }
        return position;
    }

    //endregion

    //region UI updates

    private void displayStoryInfo() {
        // TODO put something here
    }

    private void displayCurrentMediaInfo() {
        if (txtStoryName != null) {
            final String text = getString(R.string.txt_story_index,
                    mStoryIndex + 1, mCurrentMediaIndex + 1);
            txtStoryName.post(() -> txtStoryName.setText(text));
        }
    }

    private void toggleSurface(boolean showVideo) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {
                if (showVideo) {
                    imgPicture.setImageDrawable(null);
                    imgPicture.setVisibility(View.INVISIBLE);
                    surfaceVideo.setVisibility(View.VISIBLE);
                } else {
                    surfaceVideo.setVisibility(View.INVISIBLE);
                    imgPicture.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    /**
     * Display a loading indicator. Currently uses the corresponding
     * playback progress bar, toggling its indeterminate mode.
     * @param isLoading
     */
    private void toggleLoadingProgress(boolean isLoading) {
        ProgressBar progress = getCurrentProgressBar();
        final int relatedIndex = mCurrentMediaIndex;
        if (progress != null) {
            getActivity().runOnUiThread(() -> {
                int duration = getResources().getInteger(android.R.integer.config_shortAnimTime);
                progress.animate().alpha(0.5F).setDuration(duration);
                progress.postDelayed(() -> {
                    if (mCurrentMediaIndex != relatedIndex) {
                        // current media index has been changed since
                        return;
                    }
                    progress.setIndeterminate(isLoading);
                    progress.animate().alpha(1F).setDuration(duration);
                }, duration);
            });
            progress.post(() -> {
                progress.setIndeterminate(isLoading);
            });
        }
    }

    /**
     * Inflate and display the required number of progress bars for this story.
     * Aborts if story data not loaded yet, or if progress bars already initialized.
     */
    private void initProgressBars() {

        if (layoutBars == null) {
            // view hierarchy not inflated
            return;
        }

        if (getActivity() == null) {
            Log.d(makeLogTag(), "initProgressBars aborting: fragment detached");
            return;
        }
        if (!hasAnyMedia()) {
            Log.d(makeLogTag(), "initProgressBars aborting: no media (not yet loaded?)");
        }

        final LayoutInflater inflater = getActivity().getLayoutInflater();
        layoutBars.post(() -> {
            layoutBars.removeAllViews();
            for (int i = 0; i < mMedia.size(); i++) {
                // TODO refactor
                FrameLayout view = (FrameLayout) inflater.inflate(R.layout.view_bar, null, false);
                ProgressBar bar = (ProgressBar) view.findViewById(R.id.bar);
                view.removeView(bar);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(bar.getLayoutParams());
                lp.weight = 1;
                int margin = getResources().getDimensionPixelSize(R.dimen.pad_small);
                lp.setMargins(margin, 0, margin, 0);
                bar.setLayoutParams(lp);
                layoutBars.addView(bar);
            }
        });

    }

    private ProgressBar getCurrentProgressBar() {
        if (layoutBars == null) {
            // view hierarchy not inflated
            return null;
        }
        return (ProgressBar) layoutBars.getChildAt(mCurrentMediaIndex);
    }

    /**
     * Make the current progress bar filled (progress = 100%)
     */
    private void fillProgressBar(int index, boolean animate) {

        if (layoutBars == null) {
            // view hierarchy not inflated
            return;
        }

        Log.v(makeLogTag(), makeLogMessage("filling progress bar at index " + index));
        final ProgressBar progressBar = (ProgressBar) layoutBars.getChildAt(index);
        if (progressBar != null) {
            progressBar.post(() -> {
                progressBar.animate().alpha(1F);
                progressBar.setIndeterminate(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    progressBar.setProgress(FULL_PROGRESS, animate);
                } else {
                    progressBar.setProgress(FULL_PROGRESS);
                }
            });
        }
    }

    private void emptyProgressBar(int index, boolean animate) {

        Log.v(makeLogTag(), makeLogMessage("emptying progress bar at index " + index));

        if (layoutBars == null) {
            // view hierarchy not inflated
            return;
        }

        final ProgressBar progressBar = (ProgressBar) layoutBars.getChildAt(index);
        if (progressBar != null) {
            progressBar.post(() -> {
                progressBar.animate().alpha(1F);
                progressBar.setIndeterminate(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    progressBar.setProgress(EMPTY_PROGRESS, animate);
                } else {
                    progressBar.setProgress(EMPTY_PROGRESS);
                }
            });
        }

    }

    private void emptyAllBars() {

        if (layoutBars == null) {
            // view hierarchy not inflated
            return;
        }

        layoutBars.post(() -> {
            for (int i = 0; i < layoutBars.getChildCount(); i++) {
                ((ProgressBar) layoutBars.getChildAt(i)).setProgress(EMPTY_PROGRESS);
            }
        });

    }

    //endregion

    /**
     * Initializes TimerTask for the current item.
     * Should be always called before STARTING playback of a "new" item
     * (as opposed to resuming paused playback)
     */
    private void initTimerForCurrentProgress() {
        if (hasAnyMedia()) {
            stopTimers();
            if (isCurrentMediaVideo()) {
                // timerTaskVideo.cancel();
                playbackProgressTimerTask = new VideoTimerTask(exoPlayer);
                Log.d(makeLogTag(), makeLogMessage("new VIDEO timer task created"));
            } else {
                ImageTimerTask timerTask = new ImageTimerTask(IMAGE_DISPLAY_DURATION, TIMER_PERIOD,
                        mCurrentMediaSavedPosition);
                // initially pause the image playback, onPrepared() will start it
                timerTask.togglePaused(true);
                playbackProgressTimerTask = timerTask;
                Log.d(makeLogTag(), makeLogMessage("new IMAGE timer task created"));
            }
            playbackProgressTimerTask.setListener(this);
            playbackProgressTimer.scheduleAtFixedRate(playbackProgressTimerTask, TIMER_DELAY, TIMER_PERIOD);
        }
    }

    /**
     * Stop the playbackProgressTimer for progress bar
     */
    private void stopTimers() {
        // stop video progress update
        if (playbackProgressTimerTask != null) {
            playbackProgressTimerTask.cancel();
            playbackProgressTimerTask.removeListener();
            playbackProgressTimerTask.dispose();
            playbackProgressTimerTask = null;
            Log.d(makeLogTag(), makeLogMessage("timer task cancelled"));
        }
    }

    //region Events/listeners

    /**
     * Listener for touch events
     *
     * @param view the touched view
     * @param motionEvent the dispatched event
     *
     * @return
     */
    public boolean onTouch(View view, MotionEvent motionEvent) {
        logTouch(motionEvent);
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                handleDownEvent();
                return true;
            case MotionEvent.ACTION_UP:
                handleUpEvent(view);
                return true;
            case MotionEvent.ACTION_CANCEL:
                // this happens when user swipes but there are no other users in that direction
                // so CustomViewPager didn't handle the event by navigating
                handleSwipeEvent();
                return true;
        }
        return false;
    }

    /**
     * Called by the player when video is loaded; or manually when an image is displayed
     */
    @Override
    public void onExoPrepared() {

        Log.d(makeLogTag(), makeLogMessage("EXO onPrepared"));

        MainActivity activity = getStreamActivity();
        if (getStreamActivity() == null) {
            Log.w(makeLogTag(), makeLogMessage("fragment not attached anymore; aborting playback"));
            return;
        }

        if (activity.getCurrentPage() == mStoryIndex) {
            toggleLoadingProgress(false);
            if (activity.isVisible()) {
                initTimerForCurrentProgress();
                playCurrentMedia();
            } else {
                Log.w(makeLogTag(), "activity hidden while video loading; aborting playback");
            }
        } else {
            Log.d(makeLogTag(), "this user is not visible anymore; aborting playback");
            // happens when I navigate to another user while video was loading
        }
    }

    /**
     * Called by the player when video finishes playing, or manually for image
     * (when the timer controlling image display elapses)
     */
    @Override
    public void onExoPlaybackFinished() {
        Log.d(makeLogTag(), makeLogMessage("EXO onCompletion"));
        // try go to next media in stream; stop timers if none
        if (!tryGoToNextMedia()) {
            stopTimers();
        }
    }

    @Override
    public void onExoError(final Throwable cause) {
        Log.w(makeLogTag(), makeLogMessage("EXO onError: " + cause.getMessage()));
        skipCurrentItemOnError();
    }

    /**
     * Event for when user pressed (MotionEvent.ACTION_DOWN) on the player.
     */
    private void handleDownEvent() {
        Log.d(makeLogTag(), "touch event DOWN");
        pauseCurrentMedia();
        timePressedDown = System.currentTimeMillis();
    }

    /**
     * Event for when user releases (MotionEvent.ACTION_UP) on the VideoView
     */
    private void handleUpEvent(View view) {

        Log.d(makeLogTag(), "touch event UP");
        long heldDownFor = System.currentTimeMillis() - timePressedDown;
        Log.d(makeLogTag(), "held down for " + heldDownFor + " ms");

        if (heldDownFor < PAUSE_THRESHOLD) {
            // this was a quick tap - navigate to next or previous
            Log.d(makeLogTag(), "this was a TAP");
            boolean navigationSuccessful = false;
            if (view == btnPreviousItem) {
                navigationSuccessful = rewindCurrentMedia();
                if (!navigationSuccessful) {
                    navigationSuccessful = tryGoToPreviousMedia();
                }
            } else if (view == btnNextItem) {
                navigationSuccessful = tryGoToNextMedia();
            }
            if (!navigationSuccessful && !mStoryPlaybackEnded) {
                // if no navigation took place, just resume playback
                playCurrentMedia();
            }
        } else {
            Log.d(makeLogTag(), "this was a HOLD");
            // this was a long hold that paused the playback, continue now
            playCurrentMedia();
            // Toast.makeText(getContext(), "held down for " + heldDownFor + " ms", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Handler for a swipe event that gets in effect when this fragment is the only one in sequence
     * (there's no previous/next user to navigate to); otherwise the ViewPager handles the swipe
     * and navigates to another user.
     */
    private void handleSwipeEvent() {
        // media needs to be resumed because it was already paused
        // (a swipe starts with an ACTION_DOWN, just like a tap)
        playCurrentMedia();
    }

    //endregion

    private void logTouch(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case (MotionEvent.ACTION_DOWN):
                Log.v(TAG, "motion event: ACTION_DOWN");
                break;
            case (MotionEvent.ACTION_UP):
                Log.v(TAG, "motion event: ACTION_UP");
                break;
            case (MotionEvent.ACTION_MOVE):
                Log.v(TAG, "motion event: ACTION_MOVE");
                break;
            case (MotionEvent.ACTION_CANCEL):
                Log.v(TAG, "motion event: ACTION_CANCEL");
                break;
            case (MotionEvent.ACTION_SCROLL):
                Log.v(TAG, "motion event: ACTION_SCROLL");
                break;
            case (MotionEvent.ACTION_MASK):
                Log.v(TAG, "motion event: ACTION_MASK");
                break;
            case (MotionEvent.ACTION_OUTSIDE):
                Log.v(TAG, "motion event: ACTION_OUTSIDE");
                break;
        }
    }

    private String makeLogTag() {
        return TAG + "_" + mStoryIndex;
    }

    private String makeLogMessage(String message) {
        return "[item " + (mCurrentMediaIndex + 1) + "] " + message;
    }

    @Override
    public void onTick(int progress) {
        ProgressBar currentProgress = getCurrentProgressBar();
        if (currentProgress != null) {
            currentProgress.post(() -> currentProgress.setProgress(progress));
        }
    }

    @Override
    public void onEndReached() {
        if (playbackProgressTimerTask instanceof ImageTimerTask) {
            playbackProgressTimerTask.cancel();
            tryGoToNextMedia();
        }
    }

    //endregion

    private RequestListener<Drawable> listenerGlide = new RequestListener<Drawable>() {

        @Override
        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
            skipCurrentItemOnError();
            return false;
        }

        @Override
        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
            onExoPrepared(); // act as if a VIDEO was just loaded
            return false;
        }

    };

}
