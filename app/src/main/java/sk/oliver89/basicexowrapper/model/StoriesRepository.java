package sk.oliver89.basicexowrapper.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by olive on 11/27/2017.
 */

public class StoriesRepository {

    private static final int STORIES_COUNT = 2;

    public static int getStoriesCount() {
        return STORIES_COUNT;
    }

    public static List<StoryItem> getStory(int storyId) {
        List<StoryItem> story = new ArrayList<>();
        switch (storyId) {
            case 0:
                story.add(new StoryItem(true, "http://oliver89.srve.io/misc/stories/stream_0_0.mp4"));
                story.add(new StoryItem(true, "http://oliver89.srve.io/misc/stories/stream_0_1.mp4"));
                story.add(new StoryItem(false, "http://oliver89.srve.io/misc/stories/stream_0_2.jpg"));
                story.add(new StoryItem(true, "http://oliver89.srve.io/misc/stories/stream_0_3.mp4"));
                break;
            case 1:
                story.add(new StoryItem(true, "http://oliver89.srve.io/misc/stories/stream_1_0.mp4"));
                story.add(new StoryItem(true, "http://oliver89.srve.io/misc/stories/stream_1_1.mp4"));
                story.add(new StoryItem(true, "http://oliver89.srve.io/misc/stories/stream_1_2.mp4"));
                story.add(new StoryItem(true, "http://oliver89.srve.io/misc/stories/stream_1_3.mp4"));
                story.add(new StoryItem(true, "http://oliver89.srve.io/misc/stories/stream_1_4.mp4"));
                break;
        }
        return story;
    }

}
