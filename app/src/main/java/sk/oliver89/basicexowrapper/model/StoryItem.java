package sk.oliver89.basicexowrapper.model;

/**
 * Created by olive on 11/27/2017.
 */

public class StoryItem {

    private boolean mIsVideo;
    private String mUrl;

    public StoryItem(boolean isVideo, String url) {
        mIsVideo = isVideo;
        mUrl = url;
    }

    public boolean isVideo() {
        return mIsVideo;
    }

    public String getUrl() {
        return mUrl;
    }
}
