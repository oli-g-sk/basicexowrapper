package sk.oliver89.basicexowrapper.media;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

/**
 * Created by Oliver Gašpar on 6/30/2017.
 */
public class BasicExoWrapper implements VideoRendererEventListener, ExoPlayer.EventListener {

    // basic members
    private int mLogIdentifier = -1;
    private Uri mVideoUri;
    private Listener mListener;
    private boolean isLooping;
    private boolean mWasReleased;

    // ExoPlayer related members
    private DataSource.Factory mDataSourceFactory;
    private ExtractorsFactory mExtractorsFactory;
    private SimpleExoPlayer mPlayer;
    private int mLastPlayerState = ExoPlayer.STATE_IDLE;

    private boolean mPlaybackReadyEventBroadcast;
    // TODO this shouldn't be needed but prevents playback errors after configuration change

    public BasicExoWrapper(Context context, SurfaceView surface) {

        // 1. Create a default TrackSelector
        Handler mainHandler = new Handler();
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        // 2. Create the player
        mPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector);
        mPlayer.setVideoSurfaceView(surface);

        // TODO is this needed?
        // Measures bandwidth during playback. Can be null if not required.
        // DefaultBandwidthMeter defaultBandwidthMeter = new DefaultBandwidthMeter();

        // Produces DataSource instances through which media data is loaded.
        mDataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent
                (context, "yourApplicationName"), /*defaultBandwidthMeter*/ null);
        // Produces Extractor instances for parsing the media data.
        mExtractorsFactory = new DefaultExtractorsFactory();

        mPlayer.setVideoDebugListener(this);
        // mPlayer.setAudioDebugListener(this);
        mPlayer.addListener(this);

        // TODO add support for other scaling modes than FILL (like FIT, but keeping AR)
        mPlayer.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

    }

    // region Public methods

    /**
     * Loads media at the given URI
     @param loop If true, playback will auto-rewind and loop forever
     */
    public void load(Uri mediaUri, boolean loop) {
        throwIfReleased();
        Log.d(makeLogTag(), "load; loop? " + loop + ", uri: " + mediaUri);
        mPlaybackReadyEventBroadcast = false;
        mVideoUri = mediaUri;
        MediaSource videoSource = new ExtractorMediaSource(mVideoUri,
                mDataSourceFactory, mExtractorsFactory, null, null);
        mPlayer.prepare(videoSource);
        Log.v(makeLogTag(), "player prepared with new media");
        mPlayer.setPlayWhenReady(false);
        isLooping = loop;
    }

    /**
     * Loads and starts playing media at the given URI
     * @param loop If true, playback will auto-rewind and loop forever
     */
    public void loadAndStart(Uri mediaUri, boolean loop) {
        throwIfReleased();
        Log.d(makeLogTag(), "load and start");
        load(mediaUri, loop);
        play();
    }

    public void setListener(Listener listener) {
        throwIfReleased();
        Log.v(makeLogTag(), "external listener added");
        mListener = listener;
    }

    public void removeListener() {
        throwIfReleased();
        Log.v(makeLogTag(), "external listener removed");
        mListener = null;
    }

    public boolean isPlaying() {
        throwIfReleased();
        return mPlayer.getPlayWhenReady();
    }

    public void play() {
        throwIfReleased();
        if (getCurrentPosition() == 0) {
            Log.d(makeLogTag(), "play: starting from beginning");
        } else {
            Log.d(makeLogTag(), "play: resuming from " + getCurrentPosition() + " ms");
        }
        mPlayer.setPlayWhenReady(true);
    }

    public void pause() {
        throwIfReleased();
        mPlayer.setPlayWhenReady(false);
        Log.d(makeLogTag(), "pause at " + getCurrentPosition() + " ms");
    }

    public long getCurrentPosition() {
        throwIfReleased();
        long position = 0;
        try {
            position = mPlayer.getDuration() == C.TIME_UNSET ? 0 : mPlayer.getCurrentPosition();
        } catch (IndexOutOfBoundsException ex) {
            Log.e(makeLogTag(), "IndexOutOfBoundsException occurred in getCurrentPosition; will return 0");
            // TODO try to reproduce and examine
            // STACK TRACE for Exo ver. 2.4.2
            // at com.google.android.exoplayer2.Timeline$1.getWindow(Timeline.java:107)
            // at com.google.android.exoplayer2.Timeline.getWindow(Timeline.java:161)
            // at com.google.android.exoplayer2.Timeline.getWindow(Timeline.java:148)
            // at com.google.android.exoplayer2.ExoPlayerImpl.getDuration(ExoPlayerImpl.java:279)
            // at com.google.android.exoplayer2.SimpleExoPlayer.getDuration(SimpleExoPlayer.java:621)
            // at com.cleevio.seevi.util.ExoPlayerWrapper.getCurrentPosition
        }
        return position;
    }

    public void seekTo(long positionMs) {
        throwIfReleased();
        mPlayer.seekTo(positionMs);
    }

    /**
     * Pauses and rewinds the player to the starting position
     * @param autoPlay Whether the playback should start automatically
     */
    public void rewind(boolean autoPlay) {
        throwIfReleased();
        pause();
        mPlayer.seekTo(0);
        if (autoPlay) {
            play();
        }
    }

    public int getDuration() {
        throwIfReleased();
        return mPlayer.getDuration() == C.TIME_UNSET ? 0
                : (int) mPlayer.getDuration();
    }

    public int getBufferPercentage() {
        throwIfReleased();
        return mPlayer.getBufferedPercentage();
    }

    public float getVolume() {
        throwIfReleased();
        return mPlayer.getVolume();
    }

    public void setVolume(float volume) {
        throwIfReleased();
        mPlayer.setVolume(volume);
    }

    public void release() {
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.removeListener(this);
            mPlayer.release();
            mPlayer = null;
            mWasReleased = true;
        }
        Log.d(makeLogTag(), "player released");
    }

    /**
     * Assign an optional numeric ID which will identify this player instance in the logs.
     */
    public void setLogIdentifier(int logIdentifier) {
        mLogIdentifier = logIdentifier;
    }

    //endregion

    //region Private methods

    private void throwIfReleased() {
        if (mWasReleased)
            throw new IllegalStateException("Interacting with a released (disposed) instance!");
    }

    private String makeLogTag() {
        String tag = "EXO_WRAPPER";
        if (mLogIdentifier > -1) {
            tag += "_" + mLogIdentifier;
        }
        return tag;
    }

    //endregion

    //region Event listeners

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        Log.v(makeLogTag(), "onLoadingChanged: " + isLoading);
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        String newState = "UNKNOWN";
        switch (playbackState) {
            case ExoPlayer.STATE_IDLE:
                newState = "IDLE";
                break;
            case ExoPlayer.STATE_BUFFERING:
                newState = "BUFFERING";
                break;
            case ExoPlayer.STATE_READY:
                newState = "READY";
                break;
            case ExoPlayer.STATE_ENDED:
                newState = "ENDED";
                break;
        }

        Log.v(makeLogTag(), "player state changed: " + newState);

        // ignore if the state "changed" to the same state (for example,\
        // STATE_READY might get called more than once after loading a video)
        if (mLastPlayerState == playbackState) {
            Log.v(makeLogTag(), "new state same as current - IGNORED!");
            return;
        }
        mLastPlayerState = playbackState;

        // rewind if we reached the end and player should be looping
        if (playbackState == ExoPlayer.STATE_ENDED && isLooping) {
            Log.d(makeLogTag(), "video is looping, rewinding");
            rewind(true);
        }

        // abort if no listener
        if (mListener == null) {
            Log.v(makeLogTag(), "no listener attached; state change will not be broadcast");
            return;
        }

        // inform listener
        if (playbackState == ExoPlayer.STATE_READY) {
            if (!mPlaybackReadyEventBroadcast) {
                mPlaybackReadyEventBroadcast = true;
                mListener.onExoPrepared();
                Log.d(makeLogTag(), "video prepared");
            } else {
                Log.w(makeLogTag(), ""); // TODO LOG ?
            }
        }
        if (playbackState == ExoPlayer.STATE_ENDED) {
            mListener.onExoPlaybackFinished();
            Log.d(makeLogTag(), "video playback finished");
        }

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

        Log.e(makeLogTag(), "exo: player error");

        if (error.getCause() != null) {
            Log.e(makeLogTag(), "cause: " + error.getCause());
        }

        /* TODO examine other possible error causes
        if (error.getRendererException() != null) NOTE that error.getRendererException may throw IllegalStateException
            Log.e(makeLogTag(), "renderer exception: " + error.getRendererException());
        if (error.getSourceException() != null)
            Log.e(makeLogTag(), "source exception: " + error.getSourceException());
        if (error.getUnexpectedException() != null)
            Log.e(makeLogTag(), "unexpected exception: " + error.getUnexpectedException());
        */

        if (mListener != null) {
            mListener.onExoError(error.getCause() == null ? error : error.getCause());
        }

    }

    @Override
    public void onPositionDiscontinuity() {
        Log.w(makeLogTag(), "exo: position discontinuity");
    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        Log.d(makeLogTag(), "exo: playback parameters changed: " + playbackParameters.toString());
    }

    //endregion

    //region Debug event listeners

    @Override
    public void onVideoEnabled(DecoderCounters counters) {
        Log.v(makeLogTag(), "exo: video enabled");
    }

    @Override
    public void onVideoDecoderInitialized(String decoderName, long initializedTimestampMs,
                                          long initializationDurationMs) {
        Log.v(makeLogTag(), "exo: decoder initialized; name: " + decoderName
                + ", init duration: " + initializationDurationMs);
    }

    @Override
    public void onVideoInputFormatChanged(Format format) {
        Log.v(makeLogTag(), "exo: video input format changed; format: " + format.toString());
        // TODO find out if scaling mode reset is needed here
        // source: https://developer.android.com/reference/android/media/MediaCodec.html#setVideoScalingMode(int)
        // mPlayer.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
    }

    @Override
    public void onDroppedFrames(int count, long elapsedMs) {
        Log.d(makeLogTag(), "exo: dropped frames; count: " + count + ", elapsedMs: " + elapsedMs);
    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees,
                                   float pixelWidthHeightRatio) {

    }

    @Override
    public void onRenderedFirstFrame(Surface surface) {
        Log.v(makeLogTag()," exo: rendered first frame");
    }

    @Override
    public void onVideoDisabled(DecoderCounters counters) {
        Log.v(makeLogTag(), "exo: video disabled");
    }

    //endregion

    public interface Listener {

        /**
         * Called when media is loaded
         */
        void onExoPrepared();

        /**
         * Called when playback reaches end
         */
        void onExoPlaybackFinished();

        /**
         * Called when an error occurs duringg loading or playback
         * @param cause
         */
        void onExoError(Throwable cause);

    }

}

