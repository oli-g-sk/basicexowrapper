package sk.oliver89.basicexowrapper;

import android.content.DialogInterface;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.BindView;
import butterknife.ButterKnife;
import sk.oliver89.basicexowrapper.model.StoriesRepository;

public class MainActivity extends AppCompatActivity {

    private StoriesPagerAdapter pagerAdapter;

    private boolean mIsVisible;

    @BindView(R.id.pager_stories)
    public ViewPager pagerStories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        pagerAdapter = new StoriesPagerAdapter(getSupportFragmentManager(),
                StoriesRepository.getStoriesCount());
        pagerStories.setAdapter(pagerAdapter);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        /* TODO test immersive fullscreen mode
        if (hasFocus) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                pagerStories.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            }
        } else {
            pagerStories.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
        */
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsVisible = false;
    }

    public boolean isVisible() {
        return mIsVisible;
    }

    public int getCurrentPage() {
        if (pagerStories != null) {
            return pagerStories.getCurrentItem();
        }
        return -1;
    }

    /**
     * Call this to inform the activity that a story playback has ended
     * @param where Index of the story that just ended
     */
    public void onStoryEnded(int where) {

        int nextPage = pagerStories.getCurrentItem() + 1;
        if (nextPage < pagerAdapter.getCount()) {
            // navigate to next story
            pagerStories.setCurrentItem(nextPage);
        } else {
            // no more stories left
            new AlertDialog.Builder(this)
                    .setTitle(R.string.alert_title_seen_everything)
                    .setMessage(R.string.alert_message_seen_everything)
                    .setPositiveButton(R.string.btn_exit, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .setNeutralButton(R.string.btn_restart, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            pagerStories.setCurrentItem(0);
                        }
                    })
                    .create()
                    .show();
        }

    }

}
