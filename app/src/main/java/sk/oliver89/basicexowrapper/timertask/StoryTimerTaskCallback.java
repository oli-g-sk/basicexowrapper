package sk.oliver89.basicexowrapper.timertask;

/**
 * Created by olive on 11/27/2017.
 */

public interface StoryTimerTaskCallback {

    /**
     * @param progress Current progress (not necessarily different from previous "tick")
     */
    void onTick(int progress);

    /**
     * The timer reached a playback progress of 100
     */
    void onEndReached();

}
