package sk.oliver89.basicexowrapper.timertask;

import android.util.Log;

/**
 * Created by olive on 11/27/2017.
 */

public class ImageTimerTask extends StoryTimerTask {

    private static final String TAG = VideoTimerTask.class.getCanonicalName();

    private int mTimerPeriod;
    private long mTotalDurationMs;
    private long mCurrentPositionMs;
    private boolean mIsPlaybackPaused;

    public ImageTimerTask(long totalDurationMs, int timerPeriod) {
        mTotalDurationMs = totalDurationMs;
        mTimerPeriod = timerPeriod;
    }

    public ImageTimerTask(long totalDurationMs, int timerPeriod, long initialPositionMs) {
        this (totalDurationMs, timerPeriod);
        mCurrentPositionMs = initialPositionMs;
    }

    public long getCurrentPosition() {
        return mCurrentPositionMs;
    }

    public void togglePaused(boolean isPaused) {
        mIsPlaybackPaused = isPaused;
    }

    public void rewind() {
        mCurrentPositionMs = 0;
        if (mCallback != null) {
            mCallback.onTick(0);
        }
        mIsPlaybackPaused = false;
    }

    @Override
    public void run() {
        if (mIsPlaybackPaused) {
            // Log.v("FRAGMENT_STREAM_TIMER_" + currentItemLogTag, "image playback paused, skipping update");
            return;
        }
        int progress = (int) ((mCurrentPositionMs * 100.0F) / mTotalDurationMs);
        Log.v(TAG, "progress: " + progress);
        if (mCallback != null) {
            mCallback.onTick(progress);
        }
        if (progress >= 100) {
            if (mCallback != null) {
                mCallback.onEndReached();
            }
        } else {
            mCurrentPositionMs += mTimerPeriod;
        }
    }

    @Override
    public boolean cancel() {
        Log.d(TAG, "I was cancelled");
        return super.cancel();
    }

    @Override
    public void dispose() {
        // nothing to do here
    }
}
