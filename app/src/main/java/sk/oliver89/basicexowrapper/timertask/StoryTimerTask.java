package sk.oliver89.basicexowrapper.timertask;

import java.util.TimerTask;

/**
 * Created by olive on 11/27/2017.
 */

public abstract class StoryTimerTask extends TimerTask {

    protected StoryTimerTaskCallback mCallback;

    public void setListener(StoryTimerTaskCallback callback) {
        mCallback = callback;
    }

    public void removeListener() {
        mCallback = null;
    }

    public abstract void dispose();

}
