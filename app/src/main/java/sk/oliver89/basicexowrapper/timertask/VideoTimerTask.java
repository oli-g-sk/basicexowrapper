package sk.oliver89.basicexowrapper.timertask;

import android.util.Log;

import sk.oliver89.basicexowrapper.media.BasicExoWrapper;

/**
 * Created by olive on 11/27/2017.
 */

public class VideoTimerTask extends StoryTimerTask {

    private static final String TAG = VideoTimerTask.class.getCanonicalName();

    private BasicExoWrapper mExoWrapper;

    public VideoTimerTask(BasicExoWrapper exoWrapper) {
        mExoWrapper = exoWrapper;
    }

    @Override
    public void dispose() {
        mExoWrapper = null;;
    }

    @Override
    public void run() {

        if (mExoWrapper == null) {
            Log.w(TAG, "disposed TimerTask still running!");
            return;
        }

        int progress = 0;
        if (mExoWrapper.getDuration() > 0) {
            progress = (int)
                    ((mExoWrapper.getCurrentPosition() * 100.0F)
                            / mExoWrapper.getDuration());
        }
        if (mCallback != null) {
            mCallback.onTick(progress);
            if (progress >= 100) {
                mCallback.onEndReached();
            }
        }
        Log.v(TAG, "progress: " + progress);
    }

    @Override
    public boolean cancel() {
        Log.d(TAG, "I was cancelled");
        return super.cancel();
    }

}
