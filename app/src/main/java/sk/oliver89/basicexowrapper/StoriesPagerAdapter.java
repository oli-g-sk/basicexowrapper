package sk.oliver89.basicexowrapper;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by olive on 11/27/2017.
 */

public class StoriesPagerAdapter extends FragmentStatePagerAdapter {

    private int mStoriesCount;

    public StoriesPagerAdapter(FragmentManager fm, int storiescount) {
        super(fm);
        mStoriesCount = storiescount;
    }

    @Override
    public Fragment getItem(int position) {
        return StoryFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return mStoriesCount;
    }

}
